output "database_id" {
    value = google_sql_database.database.id
}

output "instance_id" {
    value = google_sql_database_instance.instance.id
}

output "user_id" {
    value = google_sql_user.users.id
}