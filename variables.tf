variable "project" {
  type = string
}
variable "region" {
  type = string
}
variable "name" {
  type = string
}
variable "versionn" {
  type = string
}
variable "tier" {
  type = string
}
variable "deletion_protection" {
  type = bool
}
variable "password" {
  type = string
}
variable "ipv4" {
  type = bool
}