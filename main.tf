resource "google_compute_network" "private_network" {
  name = "${var.name}-network"
  auto_create_subnetworks = false
}

resource "google_compute_global_address" "private_ip_address" {
  name          = "private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = google_compute_network.private_network.id
}

resource "google_service_networking_connection" "private_vpc_connection" {
  network                 = google_compute_network.private_network.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}

resource "google_sql_database_instance" "instance" {
  name             = "${var.name}-instance"
  region           = var.region
  database_version = var.versionn
  settings {
    tier = var.tier
    ip_configuration {
      ipv4_enabled    = false
      private_network = google_compute_network.private_network.id
    }
  }

  deletion_protection  = var.deletion_protection

}

resource "google_sql_database" "database" {
  name     = "${var.name}-database"
  instance = google_sql_database_instance.instance.name
}


resource "google_sql_user" "users" {
  name     =  "${var.name}-user"
  instance = google_sql_database_instance.instance.name
  host     = "${var.name}-user.com"
  password = var.password
}
